export const state = () => ({
  counter: 0,
  UserList: [
    {
      uuid: '',
      first_name: '',
      last_name: '',
      nick_name: '',
      email: '',
      password: '',
      facebook: '',
      twitter: '',
      line: '',
      youtube: '',
      website: '',
    },
  ],
})

export const mutations = {
  increment(state) {
    state.counter++
  },
  SetListUser(state, req) {
    state.UserList = req
  },
}

export const actions = {
  fetchLabel(context) {
    context.commit('increment')
  },
  async GetListUser(context) {
    const res = await this.$axios.$get('api/getuser/')
    if (res) {
      if (res.isOk) {
        console.log(res.data)
        context.commit('SetListUser', res.data)
      } else {
        console.log(res.msg)
      }
    }
  },

  async AddUser({ dispatch }, req) {
    console.log(req.first_name)
    const res = await this.$axios.$post('api/createuser/', req)
    if (res) {
      if (!res.isOk) {
        alert(res.msg)
      }
    }
    dispatch('GetListUser')
  },

  async EditUser({ dispatch }, req) {
    console.log(req.uuid)
    const res = await this.$axios.$post('api/edituser/', req)
    if (res) {
      if (!res.isOk) {
        alert(res.msg)
      }
    }
    dispatch('GetListUser')
  },

  async DeleteUser({ dispatch }, req) {
    console.log(req.uuid)
    const res = await this.$axios.$post('api/deleteuser/', req)
    if (res) {
      if (!res.isOk) {
        alert(res.msg)
      }
    }
    dispatch('GetListUser')
  },
}
